#!/bin/sh

# Copyright (C) 2024 Red Hat, Inc.
# Written by:
#     Andrew John Hughes <gnu.andrew@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Builds the RPM on RHEL 8

NVR=${1}
USER=${2}

if test "${NVR}" = ""; then
    echo "${0} <NVR> <USER>";
    exit 1;
fi

if test "${USER}" = ""; then
    echo "${0} <NVR> <USER>";
    exit 2;
fi

METADATA="{\"osci\": {\"upstream_nvr\": \"${NVR}\", \"upstream_owner_name\": \"${USER}\"}, \"rhel-target\": \"latest\"}"
rhpkg -v build --target=java-openjdk-rhel-8-build --custom-user-metadata "${METADATA}"

# Local Variables:
# compile-command: "shellcheck build_rhel_8.sh"
# fill-column: 80
# indent-tabs-mode: nil
# sh-basic-offset: 4
# End:
