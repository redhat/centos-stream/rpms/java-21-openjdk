#!/bin/sh

# Copyright (C) 2024 Red Hat, Inc.
# Written by:
#     Andrew John Hughes <gnu.andrew@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Builds a scratch build of vanilla OpenJDK with no local patches

SEPARATE_ARCHES=${1}
CMD="--target java-openjdk-rhel-8-build --skip-nvr-check --nowait";
SUPPORTED_ARCHES="aarch64 ppc64le s390x x86_64";

if [ "x${SEPARATE_ARCHES}" = "x" ] ; then
    SEPARATE_ARCHES=0;
fi

if [ ${SEPARATE_ARCHES} -eq 1 ] ; then
	for arch in ${SUPPORTED_ARCHES}; do \
	    rhpkg -v build --arches ${arch} --scratch ${CMD} ; \
	done && brew watch-task --mine
else
    rhpkg -v build ${CMD} && brew watch-task --mine
fi

# Local Variables:
# compile-command: "shellcheck build_vanilla.sh"
# fill-column: 80
# indent-tabs-mode: nil
# sh-basic-offset: 4
# End:
